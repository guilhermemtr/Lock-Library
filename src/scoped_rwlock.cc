#include "scoped_rwlock.h"

#ifdef __RW_SCOPED_LOCK__

rw_scoped_lock::rw_scoped_lock(pthread_rwlock_t * l):
  l(l)
{
}

rw_scoped_lock::~rw_scoped_lock()
{
}

void
rw_scoped_lock::init_rwlock(pthread_rwlock_t * l)
{
#ifdef __SL_ASSERT__
  assert(!pthread_rwlock_init(l, NULL));
#else
  pthread_rwlock_init(l, NULL);
#endif
}

void
rw_scoped_lock::del_rwlock(pthread_rwlock_t * l)
{
#ifdef __SL_ASSERT__
  assert(!pthread_rwlock_destroy(l));
#else
  pthread_rwlock_destroy(l);
#endif
}

read_scoped_lock::read_scoped_lock(pthread_rwlock_t * l)
{
#ifdef __SL_ASSERT__
  assert(!pthread_rwlock_rdlock(l));
#else
  pthread_rwlock_rdlock(l);
#endif
}

read_scoped_lock::~read_scoped_lock()
{
#ifdef __SL_ASSERT__
  assert(!pthread_rwlock_unlock(l));
#else
  pthread_rwlock_unlock(l);
#endif
}

write_scoped_lock::write_scoped_lock(pthread_rwlock_t * l)
{
#ifdef __SL_ASSERT__
  assert(!pthread_rwlock_wrlock(l));
#else
  pthread_rwlock_destroy(l);
#endif
}

write_scoped_lock::~write_scoped_lock()
{
#ifdef __SL_ASSERT__
  assert(!pthread_rwlock_unlock(l));
#else
  pthread_rwlock_unlock(l);
#endif
}

#endif
