#ifndef __RW_SCOPED_LOCK__
#define __RW_SCOPED_LOCK__

#include <pthread.h>
#include <assert.h>

// If __SL_ASSERT__ is defined, each method uses assert on operations,
// and also checks if the condition variable was set.
// otherwise, no assertions are made.

class rw_scoped_lock {
 protected:
  pthread_rwlock_t * l;
 public:
  rw_scoped_lock(pthread_rwlock_t * l);
  virtual ~rw_scoped_lock();

  static void init_rwlock(pthread_rwlock_t * l);
  static void del_rwlock(pthread_rwlock_t * l);
};

class read_scoped_lock {
 private:
  pthread_rwlock_t * l;
 public:
  read_scoped_lock(pthread_rwlock_t * l);
  virtual ~read_scoped_lock();
};

class write_scoped_lock {
 private:
  pthread_rwlock_t * l;
 public:
  write_scoped_lock(pthread_rwlock_t * l);
  virtual ~write_scoped_lock();
};

#endif
