#include <stdio.h>
#include "scoped_lock.h"
#include "scoped_rwlock.h"

#define __SL_ASSERT__

pthread_mutex_t  l;
pthread_rwlock_t rwl;

void
test1()
{



}







void
setup_locks()
{
	scoped_lock::init_lock(&l);
	rw_scoped_lock::init_rwlock(&rwl);
}

void
clean_locks()
{
	scoped_lock::del_lock(&l);
	rw_scoped_lock::del_rwlock(&rwl);
}


int
main(int argc, char** argv)
{
  setup_locks();
  test1();

  clean_locks();
  return 0;
}
